
#In this project you will build a simplified, 
#one-player version of the classic board game Battleship! 
#In this version of the game, there will be a single ship 
#hidden in a random location on a 5x5 grid. The player will 
#have 10 guesses to try to sink the ship.

#create board variable

import random
board = []
#create the board by using Os for ocean
for i in range(0,5):
	#append adds the 5 rows of Os
	board.append(["O"] * 5)

#print the game so the player can see which spots they've guessed
def print_board(board):
	for i in range(len(board)):
		print " ".join(board[i])

print "Let's play!"
print_board(board)

#randomly creates a number where the ship is stored
def random_row(board):
	#board represents first row, and board -1 is the length of the column in the first row
	return random.randint(0, len(board)-1)

def random_col(board):
	return random.randint(0, len(board)-1)

ship_row = random_row(board)
ship_col = random_col(board)
guess_row = input("Guess Row:")
guess_col = ("Guess Col:")

print ship_row
print ship_col

if (guess_row == ship_row and guess_col == ship_col):
	print "Congratulations! You sank my battleship!"
else:
	if not (0 <= guess_row < len(board)) or not (0 <= guess_col < len(board)):
		print "Ooops, that's not even in the ocean."
	else:
		print "You missed my battleship!"
		board[guess_row][guess_col] = "X"
		print_board(board)
	if board[guess_row][guess_col] == "X":
		print "You guessed that one already."